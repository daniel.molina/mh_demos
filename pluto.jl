using Pluto

if length(ARGS) > 0 && !isempty(ARGS[1])
    const problem = ARGS[1] |> uppercase
    Pluto.run(notebook="Daniel_Molina-Problem_$(problem).jl")
else
    Pluto.run()
end
