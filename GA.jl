module GA
using Random
# using Debugger
eachsol(x) = eachcol(x)

"""
This function make a callback
"""
function callback_none(i, pop, fit)
end


"""
Crossover OX.
    Breeding is done by selecting a random range of parent1, and placing
    it into the empty child route (in the same place).
    Gaps are then filled in, without duplicates, in the order they appear
    in parent2.

    For example:
        parent1: 0123456789
        parent1: 5487961320

        start_pos = 0
        end_pos = 4

        unfilled child: 01234*****
        filled child:   0123458796
"""
function crossover_OX(ind1::Vector, ind2::Vector)
    ndim = length(ind1)
    
    start_pos, end_pos = randperm(ndim)[1:2]

    if (start_pos > end_pos)
        start_pos, end_pos = end_pos, start_pos
    end

    children = zero(ind1)
    children[start_pos:end_pos] = ind1[start_pos:end_pos]
    pos = end_pos

    for (i, value) in enumerate(ind2)
        if value in children
            continue
        else
            pos += 1

            if (pos > ndim)
                pos = 1
            end

            while start_pos <= pos <= end_pos
                pos = (pos + 1)

                if pos > ndim
                    pos = 1
                end
            end

            children[pos] = value
        end
    end

    return children
end

function tournament_select(fitness; k = 3)
    popsize = length(fitness)
    inds = randperm(popsize)[1:k]
    fit_ind = fitness[inds]
    best_pos = argmin(fit_ind)
    return inds[best_pos]
end

function mutation_perm(sol, p_mut = 0.1)
    ndim = length(sol)

    if rand() < p_mut
        pos1, pos2 = randperm(ndim)[1:2]
        sol[[pos1, pos2]] = sol[[pos2, pos1]]
    end

    return sol
end

"""
Apply the GA using the permutation model by default
"""
function optimize(feval, numevals; crossover = crossover_OX, mutation = mutation_perm,
             selection = tournament_select, popsize = missing, ndim = missing, pop = missing,
                  fits = missing, callback = callback_none)
    @assert ((ismissing(ndim) && ismissing(popsize)) || (ismissing(pop))) "Error, ndim and pop must be defined"

    if (ismissing(pop))
        pop = hcat([randperm(ndim) for _ in 1:popsize]...)
    else
        if ismissing(ndim)
            ndim = size(pop, 1)
        else
            @assert ndim == size(pop, 1)
        end
    end

    if ismissing(fits)
        fits = [feval(sol) for sol in eachsol(pop)]
        evals = popsize
    else
        evals = 0
    end

    i = 1

    while (evals < numevals)
        p1 = selection(fits)
        p2 = selection(fits)

        while p2 == p1
            p2 = selection(fits)
        end

        children = crossover(pop[:, p1], pop[:, p2])
        children = mutation(children)
        fit_children = feval(children)
        evals += 1
        worst = argmax(fits)

        if (fits[worst] > fit_children)
            pop[:, worst] = children
            fits[worst] = fit_children
        end

        callback(i, pop, fits)
        i += 1
    end
    return pop, fits
end

end
