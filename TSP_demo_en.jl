### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ 1a25621c-fe89-4e77-9d7f-3f6a1fbf89d3
begin
	using DelimitedFiles
	using PlutoUI
	using Plots
	using Random
	gr();
end

# ╔═╡ 05459fde-1076-11eb-2137-e5ef5ea40f15
include("./GA.jl")

# ╔═╡ 51c1c60e-0bca-11eb-1474-352473905401
md"# Ejemplo de uso de Algoritmo Genético para resolver el problema del TSP"

# ╔═╡ 961145f4-0bcb-11eb-34da-4111490f3aa8
md" Leemos el fichero cities, que contiene las posiciones x e y de cada ciudad"

# ╔═╡ 4d8dd1f6-0bcb-11eb-3885-5d69039314c9
	function read_data(value::Int)
		fname  = joinpath("data", "gr$(value).tsp")
		data = readdlm(fname, skipstart=7, comment_char='E', comments=true) |> Matrix
		cities = data[:, 2:end]
    	return cities
	end

# ╔═╡ fef47441-4ddf-4637-8dd9-2610840134e3
begin 
	html  = @bind value Select(["96", "137", "202", "229", "431", "666"])
	md"Elige el tamaño del TSP: $html"
end

# ╔═╡ b2bc53ca-1073-11eb-0af7-a55323435932
cities = read_data(parse(Int, value))

# ╔═╡ dfe95b94-1073-11eb-0c5f-e9ddbecb3776
md"""
## Funciones de evaluación

La evaluación será la suma de las distancias euclídeas
"""

# ╔═╡ ecd2b164-1073-11eb-1a08-ffbbb817c143
dist(a,b)=sqrt(sum((a .- b).^2))

# ╔═╡ ad305557-09c0-4b5e-abaf-6fad41dba75a
md"""
Y usando eso se define fitness como la suma euclídea desde una ciudad a la siguiente (y volver a la primera)
"""

# ╔═╡ f239e25a-1073-11eb-1c74-1f54dedd6a13
function fitness(sol)
    total = 0.0
	global cities
	
    for i = 1:size(sol, 1)-1
        total += dist(cities[sol[i], :], cities[sol[i+1], :])
    end
    
	total += dist(cities[sol[end],:], cities[sol[begin],: ])
    return total
end

# ╔═╡ f95ace62-1073-11eb-1735-9d2f0fcfc229
md"""
Vamos a visualizar las ciudades primero
"""

# ╔═╡ 4fb74e88-0bcf-11eb-2855-0f81a6b52f7e
md"### Visualizando un camino"

# ╔═╡ d8e8ad06-1074-11eb-3524-effb1a0ee622
begin
const ndim=size(cities, 1)
	md"Existen $ndim ciudades"
end

# ╔═╡ 28cd88c4-1074-11eb-3275-ffd9ecf660ee
function plot(; sol=Missing, cities=cities, title="")
	if isempty(title)
		if sol == Missing
			title = "Ciudades a recorrer"
		else
			title = "Fitness: $(fitness(sol))"
		end
	end
    
    
	fig = Plots.scatter(cities[:,1], cities[:,2], legend=false, title=title, 	grid=false, showaxis=false, ticks=false, markersize=3)
	if  sol != Missing
		for i in 1:(ndim-1)
			extreme = [sol[i], sol[i+1]]
			Plots.plot!(fig, cities[extreme,1], cities[extreme,2], color=:black)
		end
		extreme = [sol[end], sol[begin]]
		Plots.plot!(fig, cities[extreme,1], cities[extreme,2], color=:black)
	end
	return fig
end

# ╔═╡ 88ab6ad6-0bce-11eb-1a32-db915e283f72
plot()

# ╔═╡ e0e82e1e-1074-11eb-3794-b5f8f753b623
md"Creamos una solución aleatoria, que consiste en una permutación de ciudades."

# ╔═╡ 06cc7e8a-1075-11eb-1408-93341eca6bfd
sol = shuffle(1:ndim);

# ╔═╡ 096fba78-1075-11eb-09bd-5994a6b4560a
md"Ahora la visualizamos:"

# ╔═╡ 16473fdc-1075-11eb-174d-b9ab6ba147df
plot(sol=sol)

# ╔═╡ 1b1054a4-1075-11eb-2cb3-39fecbebe537
md"Podemos generar tantas como queramos:"

# ╔═╡ 33956b86-1075-11eb-28a3-59fa289c3bdb
@bind nuevo Button("Nueva Solución")

# ╔═╡ 69381996-1075-11eb-3725-d37c4baae8ea
begin
	nuevo
	plot(sol=shuffle(1:ndim))
end

# ╔═╡ d790cd22-1075-11eb-1c3e-5ff50d8924d9
md"""
# Optimizando con algoritmo evolutivo


Primero cargamos el algoritmo Evolutivo
"""

# ╔═╡ e18cd12a-1075-11eb-2650-376ad8fc05ff
function ingredients(path::String)
	# this is from the Julia source code (evalfile in base/loading.jl)
	# but with the modification that it returns the module instead of the last object
	name = Symbol(basename(path))
	m = Module(name)
	Core.eval(m,
        Expr(:toplevel,
             :(eval(x) = $(Expr(:core, :eval))($name, x)),
             :(include(x) = $(Expr(:top, :include))($name, x)),
             :(include(mapexpr::Function, x) = $(Expr(:top, :include))(mapexpr, $name, x)),
             :(include($path))))
	m
end

# ╔═╡ 24d473d8-1077-11eb-051c-a936dccce261
function conv_tsp(num_itera)
	conv = Float64[]
	index = Int[]
	
	function callback_conv(i, pop, fits)
    	push!(index, i)
    	push!(conv, minimum(fits))
	end
	
	pop, fits = GA.optimize(fitness, num_itera+10, ndim=ndim, popsize=10, 
		callback=callback_conv)
	
	return Plots.plot(index, conv, legend=false, title="Convergencia tras $(length(index)) iteraciones")
end

# ╔═╡ 17e29df4-1080-11eb-0140-211ecdfd55e4
md"""
## Visualizando las soluciones

Ahora vamos a visualizar las soluciones de una ejecución
"""

# ╔═╡ 2f762bea-1080-11eb-25ad-8bff0c7a5d91
function solutions_tsp(num_itera)
	bests = Array{Int64, 1}[]
	
	function callback_bests(i, pops, fits)
        pos = argmin(fits)
    	push!(bests, pops[:,pos])
	end
	
	pop, fits = GA.optimize(fitness, num_itera+10, ndim=ndim, popsize=10, 
		callback=callback_bests)
	return bests
end

# ╔═╡ 387fab08-1083-11eb-3438-dbb3bf8d3e8f
begin 
	field = @bind max_itera NumberField(10_000:30_000)
	md"Número de Evaluaciones: $field"
end

# ╔═╡ d1d67130-1081-11eb-12eb-99991854c12a
bests = solutions_tsp(max_itera)

# ╔═╡ daf454d6-1080-11eb-1a56-819a80d6a5e4
@bind index_sol Slider(1:max_itera)

# ╔═╡ de532692-1081-11eb-02e4-33bf301c95f9
begin
	title = "Iteración: $(index_sol), fitness: $(fitness(bests[index_sol]))"
plot(sol=bests[index_sol], title=title)
end

# ╔═╡ 114b3db6-1076-11eb-0393-63c47d74cfa1
md"## Visualizamos el aprendizaje"

# ╔═╡ 563f1546-1076-11eb-10a9-d35224fb6ba6
md"Evaluamos el AG durante un cierto número de iteraciones y visualizamos cómo mejora el fitness con las evaluaciones"

# ╔═╡ 65051f2e-1078-11eb-392a-b7a0a8146d5b
@bind num_itera Slider(1:max_itera)

# ╔═╡ e51c006e-1077-11eb-0f45-879ad2ff78d9
conv_tsp(num_itera)

# ╔═╡ Cell order:
# ╟─51c1c60e-0bca-11eb-1474-352473905401
# ╟─1a25621c-fe89-4e77-9d7f-3f6a1fbf89d3
# ╟─961145f4-0bcb-11eb-34da-4111490f3aa8
# ╟─4d8dd1f6-0bcb-11eb-3885-5d69039314c9
# ╟─fef47441-4ddf-4637-8dd9-2610840134e3
# ╟─b2bc53ca-1073-11eb-0af7-a55323435932
# ╟─dfe95b94-1073-11eb-0c5f-e9ddbecb3776
# ╟─ecd2b164-1073-11eb-1a08-ffbbb817c143
# ╟─ad305557-09c0-4b5e-abaf-6fad41dba75a
# ╟─f239e25a-1073-11eb-1c74-1f54dedd6a13
# ╟─f95ace62-1073-11eb-1735-9d2f0fcfc229
# ╟─28cd88c4-1074-11eb-3275-ffd9ecf660ee
# ╠═88ab6ad6-0bce-11eb-1a32-db915e283f72
# ╟─4fb74e88-0bcf-11eb-2855-0f81a6b52f7e
# ╟─d8e8ad06-1074-11eb-3524-effb1a0ee622
# ╟─e0e82e1e-1074-11eb-3794-b5f8f753b623
# ╟─06cc7e8a-1075-11eb-1408-93341eca6bfd
# ╟─096fba78-1075-11eb-09bd-5994a6b4560a
# ╟─16473fdc-1075-11eb-174d-b9ab6ba147df
# ╟─1b1054a4-1075-11eb-2cb3-39fecbebe537
# ╟─33956b86-1075-11eb-28a3-59fa289c3bdb
# ╟─69381996-1075-11eb-3725-d37c4baae8ea
# ╟─d790cd22-1075-11eb-1c3e-5ff50d8924d9
# ╟─e18cd12a-1075-11eb-2650-376ad8fc05ff
# ╟─05459fde-1076-11eb-2137-e5ef5ea40f15
# ╟─24d473d8-1077-11eb-051c-a936dccce261
# ╟─17e29df4-1080-11eb-0140-211ecdfd55e4
# ╟─2f762bea-1080-11eb-25ad-8bff0c7a5d91
# ╟─387fab08-1083-11eb-3438-dbb3bf8d3e8f
# ╟─d1d67130-1081-11eb-12eb-99991854c12a
# ╟─daf454d6-1080-11eb-1a56-819a80d6a5e4
# ╟─de532692-1081-11eb-02e4-33bf301c95f9
# ╟─114b3db6-1076-11eb-0393-63c47d74cfa1
# ╟─563f1546-1076-11eb-10a9-d35224fb6ba6
# ╟─e51c006e-1077-11eb-0f45-879ad2ff78d9
# ╟─65051f2e-1078-11eb-392a-b7a0a8146d5b
